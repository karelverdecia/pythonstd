import _ from 'lodash';
import colors from 'colors/safe';
import * as parameters from './parameters';
import { PyParams, _get_py_params } from './parameters';
import * as exceptions from './exceptions';
import { list, List }  from './list';
import { dict, Dict }  from './list';

const { AttributeError, TypeError } = exceptions;



export function int(value) {
    value = parameters._get_py_param(value);
    return _.toInteger(value);
}


export function float(value) {
    value = parameters._get_py_param(value);
    return _.toNumber(value);
}


export function str(value) {
    value = parameters._get_py_param(value);
    if (_.isObject(value)) {
        var __str__ = getattr(value, '__str__');
        if (_.isFunction(__str__))
            return __str__.bind(value)();
    }
    return _.toString(value);
}


export function unicode(value) {
    value = parameters._get_py_param(value);
    if (_.isObject(value)) {
        var __unicode__ = getattr(value, '__unicode__');
        if (_.isFunction(__unicode__))
            return __unicode__.bind(value)();
    }
    return str(value);
}


export function println(value) {
    value = parameters._get_py_param(value);
    if (value === false)
        console.error(colors.red(value));
    else
        console.log(value);
    console.log('\n');
}


export function abs(value) {
    value = parameters._get_py_param(value);
    return Math.abs(value)
}




export function hasattr(value, attribute) {
    value = parameters._get_py_param(value);
    var attrValue = value[attribute];
    return !_.isUndefined(attrValue);
}


export function getattr(value, attribute, defaultValue) {
    if (_.isNull(value) || _.isUndefined(value))
    {
        if (_.isUndefined(defaultValue))
            throw new AttributeError("Null object has no attribute "+attribute);
        return defaultValue;
    }
    var result = value[attribute];
    if (_.isUndefined(result)) {
        if (_.isUndefined(defaultValue))
            throw new AttributeError("Object has no attribute "+attribute);
        return defaultValue;
    }
    return result;
}


export function setattr(obj, attr, value) {
    obj[attr] = value;
}


export function open(file,encoding){
    var reader = new FileReader();

    reader.onload = function(e) {
        var text = reader.result;
    };
    reader.readAsText(file, encoding);

}


function *izip_max() {
    let tmp = Array.from(arguments);
    tmp.shift();
    let iterators = [];
    for (let item of arguments) {
        iterators.push(item[Symbol.iterator]());
    }
    let hasNext = true;
    while (hasNext) {
        hasNext = false;
        let result = [];
        for (let it of iterators) {
            let value = it.next();
            if (!value.done) {
                hasNext = true;
                result.push(value.value);
            }
            else
                result.push(null);
        }
        if (hasNext)
            yield result;
    }
}


export function map(fn) {
    let args;
    [ fn, args ] = _get_py_params({ fn }, { }, { args: true }, arguments);
    let result = list();
    for (let item of izip_max.apply(null, args)) {
        let r = fn.apply(null, item);
        if (_.isUndefined(r))
            r=null;
        result.append(r);
    }
    return result;
}


export function range(start, stop=null, step=null) {
    if(start instanceof PyParams)
        [ start, stop, step ] = _get_py_params({ start, stop, step }, { stop: null, step: null },{ args: true, kwarg: false }, arguments);
    if (stop === null) {
        // one param defined
        stop = start;
        start = 0;
    }

    if (step === null) {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }
    return list(result);
}

export function filter(lambda,iterable) {
    let result = list();
    for(let element of iterable)
    {
        let evaluation = lambda(element);
        if(evaluation)
            result.append(element);
    }
    return result;
}

export function reduce(fn,iterable,value=null) {
    if(_.isArray(iterable) ||  _.isMap(iterable))
        if(_.size(iterable) ==0)
            throw  new TypeError("reduce() of empty sequence with no initial value");
    if(iterable instanceof List || iterable instanceof Dict)
        if(iterable.__len__()==0)
        throw  new TypeError("reduce() of empty sequence with no initial value");


    let  iterm =0;
    let result;
    for (let value of iterable) {
        if(iterm==0)
            result = value;
        else
            result = fn(value,result);
        iterm=iterm+1;
    }
    if(value !=null)
        result = fn(result,value);
    return result;
}
