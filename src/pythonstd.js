require('babel-polyfill');
import _ from 'lodash'

(function() {

    const PythonStd = {};


    class Exception extends Error {
        constructor() {
            super();
            this.arguments = arguments;
        }
    }
    PythonStd.Exception = Exception;


    class IndexError extends Exception {}
    PythonStd.IndexError = IndexError

    class KeyError extends Exception {}
    PythonStd.KeyError = KeyError

    class AttributeError extends Exception {}
    PythonStd.AttributeError = AttributeError

    class TypeError extends Exception {}
    PythonStd.TypeError = TypeError

    class ValueError extends Exception {}
    PythonStd.ValueError = ValueError

    class ZeroDivisionError extends Exception {}
    PythonStd.ZeroDivisionError = ZeroDivisionError

    class PyParams {
        constructor(data) {
            let params = data.params || [];
            if (params instanceof List)
                params = params.list;
            let args = data.args || [];
            if (args instanceof List)
                args = args.items();
            this.args = _.concat(params, args);
            let keys = data.keys || {};
            let kwargs = data.kwargs || {};
            if (!_.isPlainObject(keys) && !(keys instanceof Dict))
                throw new TypeError("PyParam key must be a plain object or a PyDict.");
            if (!_.isPlainObject(kwargs) && !(kwargs instanceof Dict))
                throw new TypeError("PyParam kwargs must be a plain object or a PyDict.");
            if (!(keys instanceof Dict))
                keys = dict(keys);
            if (!(kwargs instanceof Dict))
                kwargs = dict(kwargs);
            for (let key of keys) {
                if (kwargs.has(key))
                    throw new TypeError("Function got multiple values for keyword argument "+key);
            }
            this.kwargs = keys;
            this.kwargs.update(kwargs);
        }

        pop(name, defvalue) {
            var result;
            if (this.args.length) {
                if (this.kwargs.has(name))
                    throw new TypeError("Function got multiple values for keyword argument"+name);
                result = this.args[0];
                this.args = _.drop(this.args);
                return result
            }
            if (!this.kwargs.has(name)) {
                if (_.isUndefined(defvalue))
                    throw new TypeError("Missing value for argument " + name);
                return defvalue;
            }
            return this.kwargs.pop(name);
        }

        hasKeyword(name) {
            return !_.isUndefined(this.kwargs[name]);
        }

        first() {
            return this.args[0];
        }

        getVarArgs() {
            return this.args;
        }

        getKwArgs() {
            return this.kwargs;
        }
    }
    PythonStd.PyParams = PyParams


    function _py_params(data) {
        return new PyParams(data);
    }
    PythonStd._py_params = _py_params;

    function _get_py_param(value) {
        if (value instanceof PyParams)
            return value.first();
        return value;
    }
    PythonStd._get_py_param = _get_py_param

    function _get_py_params(params, defaults, extra, _arguments) {
        if (!_.isPlainObject(params))
            throw new TypeError("Wrong parameters for _get_simple_params function.");
        if (!_.isPlainObject(defaults))
            throw new TypeError("Wrong parameters for _get_simple_params function.");
        if (!_.isPlainObject(extra))
            throw new TypeError("Wrong parameters for _get_simple_params function.");
        extra = extra || {};
        let result = [];
        if (_arguments[0] instanceof PyParams) {
            let pyParams = _arguments[0];
            _.forOwn(params, (value, key) => {
                result.push(pyParams.pop(key, defaults[key]));
            });
            if (extra.args) {
                result.push(pyParams.getVarArgs());
            }
            if (extra.kwargs) {
                result.push(pyParams.getKwArgs());
            }
        }
        else {
            _.forOwn(params, (value, key) => {
                if (_.isUndefined(value)) {
                    if (_.isUndefined(defaults[key]))
                        throw new TypeError("The function has received not enough arguments.");
                    value = defaults[key];
                }
                result.push(value);
            });
            if (extra.args) {
                let args = [];
                for (let i = result.length; i < _arguments.length; i++) {
                    args.push(_arguments[i]);
                }
                result.push(args);
            }
        }
        return result;
    }
    PythonStd._get_py_params = _get_py_params


    function getattr(value, attribute, defaultValue) {
        if (_.isNull(value) || _.isUndefined(value))
        {
            if (_.isUndefined(defaultValue))
                throw new AttributeError("Null object has no attribute "+attribute);
            return defaultValue;
        }
        var result = value[attribute];
        if (_.isUndefined(result)) {
            if (_.isUndefined(defaultValue))
                throw new AttributeError("Object has no attribute "+attribute);
            return defaultValue;
        }
        return result;
    }
    PythonStd.getattr = getattr


    function len(value) {
        value = _get_py_param(value);
        if (_.isArrayLike(value))
            return value.length;
        if (_.isMap(value))
            return value.size;
        if (_.isSet(value))
            return value.size;
        if (_.isFunction(value['__len__']))
            return value['__len__'].bind(value)(value);
        throw new AttributeError("Object has no method __len__.");
    }
    PythonStd.len = len;


    function _eq(v1, v2) {
        var __eq__ = getattr(v1, '__eq__', null);
        if (_.isFunction(__eq__))
            return __eq__.bind(v1)(v2);
        var __eq__ = getattr(v2, '__eq__', null);
        if (_.isFunction(__eq__))
            return __eq__.bind(v2)(v1);
        return v1 === v2;
    }
    PythonStd._eq = _eq;


    function _ne(v1, v2) {
        return !_eq(v1, v2);
    }
    PythonStd._ne = _ne;


    String.prototype.__len__ = function() {
        return this.length;
    };


    String.prototype.__getitem__ = function(key) {
        if (!_.isInteger(key))
            throw new TypeError("Array keys must be integers.", key);
        var _key = key;
        if (_key < 0)
            _key += this.length;
        if (_key < 0 || _key>=this.length)
            throw new IndexError("Index error", key);
        return this[_key];
    };


    String.prototype.__contains__ = function(value) {
        return this.indexOf(value) > -1;
    };


    String.prototype.__str__ = function() {
        return this;
    };


    String.prototype.__unicode__ = function() {
        return this;
    };


    Array.prototype.__len__ = function() {
        return this.length;
    };


    Array.prototype.__getitem__ = function(key) {
        if (!_.isInteger(key))
            throw new TypeError("Array keys must be integers.", key);
        var _key = key;
        if (_key < 0)
            _key += this.length;
        if (_key < 0 || _key>=this.length)
            throw new IndexError("Index error", key);
        return this[_key];
    };


    Array.prototype.__setitem__ = function(key, value) {
        if (!_.isInteger(key))
            throw new TypeError("Array keys must be integers.", key);
        this[key] = value;
    };


    Array.prototype.__contains__ = function(value) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === value)
                return true;
        }
        return false;
    };


    Array.prototype.__eq__ = function(value) {
        if (value instanceof Array) {
            if (this.length != value.length)
                return false;
            for (var i = 0; i < this.length; i++) {
                if (!_eq(this[i], value[i]))
                    return false;
            }
            return true;
        }
        return false;
    };


    Array.prototype.__add__ = function(value) {
        if (value instanceof Array) {
            var result = [];
            _.forEach(this, function(v) {result.push(v)});
            _.forEach(value, function(v) {result.push(v)});
            return result;
        }
        throw new TypeError("Cannot concat an array with a value of other type.");
    };

    Array.prototype.append = function(value) {
        value = _get_py_param(value);
        this.push(value);
        return this;
    };

    Array.prototype.extend = function(value) {
        value = _get_py_param(value);
        if (value instanceof Array) {
            Array.prototype.push.apply(this,value);
        }
        else{
            throw new TypeError("TypeError: "+ type_value +" object is not iterable");
        }

    };

    Array.prototype.__mul__ = function (value) {
        if (_.isInteger(value)) {
            var result = [];
            for (var i = 0; i < value; i++) {
                _.forEach(this, function(v) { result.push(v); });
            }
            return result;
        }
        throw new TypeError("Cannot multiply an array by a non integer valuer");
    };


    Map.prototype.__len__ = function() {
        return this.size;
    };


    Map.prototype.__getitem__ = function(key) {
        var result = this.get(key);
        if (_.isUndefined(result))
            throw new KeyError("Object has no key", index);
        return result;
    };


    Map.prototype.__setitem__ = function(key, value) {
        this.set(key, value);
    };


    Map.prototype.__delitem__ = function (key) {
        var result = this.delete(key);
        if (!result)
            throw new KeyError("Dictionary has no key", key);
        this.delete(key);
    };


    Map.prototype.__contains__ = function(key) {
        return this.has(key);
    };


    Set.prototype.__len__ = function() {
        return this.size;
    };


    Set.prototype.__contains__ = function(value) {
        return this.has(value);
    };


    function _item(value, index) {
        var __getitem__ = getattr(value, '__getitem__', null);
        if (_.isFunction(__getitem__))
            return __getitem__.bind(value)(index);
        var result = value[index];
        if (_.isUndefined(result))
            throw new KeyError("Object has no key "+index);
        return result;
    }
    PythonStd._item = _item


    function _setitem(array_object,index, value){
        array_object.__setitem__(index,value);
    }
    PythonStd._setitem = _setitem


    function _div(value1, value2, isInt) {
        if(_.isNumber(value1) && _.isNumber(value2))
            if(value2!=0)
            {
                if (isInt === undefined){return value1 / value2;}
                else {
                    var result = value1/value2;
                    if(result<0)
                        result=result-1;
                    return Number(result.toFixed(0));
                }

            }
            else
                throw new ZeroDivisionError("Integer division or modulo by zero")
        else{
            var type_v1=  typeof(value1);
            var type_v2=  typeof(value2);
            throw new TypeError("Unsupported operand type(s) for /:", type_v1+" and " +type_v2);
        }
    }
    PythonStd._div = _div


    function _setslice(objectList,begin,end,values){
       return  objectList.__setslice__(begin,end,values);
    }
    PythonStd._setslice = _setslice


    function _slice(objectList,start, stop, step) {
        if(_.isString(objectList)){
            var result = _.slice(objectList, start, stop, step);
            if (_.isString(objectList))
                return _.join(result, '');
            return result;
        }
        else{
            return objectList.__getslice__(start,stop,step);
        }
    }
    PythonStd._slice = _slice


    function _add(value1, value2) {
        var __add__ = getattr(value1, '__add__', null);
        if (_.isFunction(__add__))
            return __add__.bind(value1)(value2);
        return value1 + value2;
    }
    PythonStd._add = _add


    function _sub(value1, value2) {
        return value1 - value2;
    }
    PythonStd._sub = _sub


    function _mul(value1, value2) {
        var __mul__ = getattr(value1, '__mul__', null);
        if (_.isFunction(__mul__))
            return __mul__.bind(value1)(value2);
        var __mul__ = getattr(value2, '__mul__', null);
        if (_.isFunction(__mul__))
            return __mul__.bind(value2)(value1);

        var text = null;
        var count = null;
        if (_.isString(value1) && _.isInteger(value2)) {
            text = value1;
            count = value2;
        }
        if (_.isInteger(value1) && _.isString(value2)) {
            text = value2;
            count = value1;
        }
        if (!_.isNull(text)) {
            return _.repeat(text, count);
        }
        return value1 * value2;
    }
    PythonStd._mul = _mul


    function _mod(value1, value2) {
        return value1 % value2;
    }
    PythonStd._mod = _mod


    function _isTrue(value) {
        if (value === 0)
            return false;
        if (value === "")
            return false;
        if (value === false)
            return false;
        if (value === null)
            return false;
        if (value === undefined)
            return false;
        if (value instanceof Array || _.isPlainObject(value) || value instanceof Set || value instanceof Map)
            return !_.isEmpty(value);
        if (_.isFunction(value['__nonzero__']))
            return value['__nonzero__'].bind(value)();
        if (_.isFunction(value['__len__']))
            return value['__len__'].bind(value)();
        return true;
    }
    PythonStd._isTrue = _isTrue


    function _not(value) {
        return !_isTrue(value);
    }
    PythonStd._not = _not


    function _lt(value1, value2) {
        try{
            return value1<value2;
        }
        catch(e)
        {
            var type_v1=  typeof(value1);
            var type_v2=  typeof(value2);
            throw new TypeError("Unsupported operand type(s) for <", type_v1+" and " +type_v2);
        }
    }
    PythonStd._lt = _lt;


    function _gt(value1, value2) {
        try{
            return value1>value2;
        }
        catch(e)
        {
            var type_v1=  typeof(value1);
            var type_v2=  typeof(value2);
            throw new TypeError("Unsupported operand type(s) for >:", type_v1+" and " +type_v2);
        }
    }
    PythonStd._gt = _gt;


    function _lte(value1, value2) {
        try{
            return value1<=value2;
        }
        catch(e)
        {
            var type_v1=  typeof(value1);
            var type_v2=  typeof(value2);
            throw new TypeError("Unsupported operand type(s) for <=:", type_v1+" and " +type_v2);
        }
    }
    PythonStd._lte = _lte;


    function _gte(value1, value2) {
        try{
            return value1>=value2 ;
        }
        catch(e)
        {
            var type_v1=  typeof(value1);
            var type_v2=  typeof(value2);
            throw new TypeError("Unsupported operand type(s) for >=:", type_v1+" and " +type_v2);
        }
    }
    PythonStd._gte = _gte;

    function _in(value, obj) {
        if (obj instanceof Set || obj instanceof Map)
            return obj.has(value);
        if (obj instanceof Array) {
            for (let item of obj) {
                if (_eq(item, value))
                    return true;
            }
            return false;
        }
        if (obj['__contains__'])
            return obj['__contains__'](value);
        return false;
    }
    PythonStd._in = _in;




    function int(value) {
        value = _get_py_param(value);
        return _.toInteger(value);
    }
    PythonStd.int = int;


    function float(value) {
        value = _get_py_param(value);
        return _.toNumber(value);
    }
    PythonStd.float = float;


    function str(value) {
        value = _get_py_param(value);
        if (_.isObject(value)) {
            var __str__ = getattr(value, '__str__');
            if (_.isFunction(__str__))
                return __str__.bind(value)();
        }
        return _.toString(value);
    }
    PythonStd.str = str;


    function unicode(value) {
        value = _get_py_param(value);
        if (_.isObject(value)) {
            var __unicode__ = getattr(value, '__unicode__');
            if (_.isFunction(__unicode__))
                return __unicode__.bind(value)();
        }
        return str(value);
    }
    PythonStd.unicode = unicode;


    function println(value) {
        value = _get_py_param(value);
        if (value === false)
            console.error(colors.red(value));
        else
            console.log(value);
        console.log('\n');
    }
    PythonStd.println = println;


    function abs(value) {
        value = _get_py_param(value);
        return Math.abs(value)
    }
    PythonStd.abs = abs;


    function hasattr(value, attribute) {
        value = _get_py_param(value);
        var attrValue = value[attribute];
        return !_.isUndefined(attrValue);
    }
    PythonStd.hasattr = hasattr;


    function getattr(value, attribute, defaultValue) {
        if (_.isNull(value) || _.isUndefined(value))
        {
            if (_.isUndefined(defaultValue))
                throw new AttributeError("Null object has no attribute "+attribute);
            return defaultValue;
        }
        var result = value[attribute];
        if (_.isUndefined(result)) {
            if (_.isUndefined(defaultValue))
                throw new AttributeError("Object has no attribute "+attribute);
            return defaultValue;
        }
        return result;
    }
    PythonStd.getattr = getattr;


    function setattr(obj, attr, value) {
        obj[attr] = value;
    }
    PythonStd.setattr = setattr;


    function open(file,encoding){
        var reader = new FileReader();

        reader.onload = function(e) {
            var text = reader.result;
        };
        reader.readAsText(file, encoding);
    }
    PythonStd.open = open;


    function *izip_max() {
        let tmp = Array.from(arguments);
        tmp.shift();
        let iterators = [];
        for (let item of arguments) {
            iterators.push(item[Symbol.iterator]());
        }
        let hasNext = true;
        while (hasNext) {
            hasNext = false;
            let result = [];
            for (let it of iterators) {
                let value = it.next();
                if (!value.done) {
                    hasNext = true;
                    result.push(value.value);
                }
                else
                    result.push(null);
            }
            if (hasNext)
                yield result;
        }
    }
    PythonStd.izip_max = izip_max;


    function map(fn) {
        let args;
        [ fn, args ] = _get_py_params({ fn }, { }, { args: true }, arguments);
        let result = list();
        for (let item of izip_max.apply(null, args)) {
            let r = fn.apply(null, item);
            if (_.isUndefined(r))
                r=null;
            result.append(r);
        }
        return result;
    }
    PythonStd.map = map;


    function range(start, stop=null, step=null) {
        if(start instanceof PyParams)
            [ start, stop, step ] = _get_py_params({ start, stop, step }, { stop: null, step: null },{ args: true, kwarg: false }, arguments);
        if (stop === null) {
            // one param defined
            stop = start;
            start = 0;
        }

        if (step === null) {
            step = 1;
        }

        if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
            return [];
        }

        var result = [];
        for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
            result.push(i);
        }
        return list(result);
    }
    PythonStd.range = range;


    function filter(lambda,iterable) {
        let result = list();
        for(let element of iterable)
        {
            let evaluation = lambda(element);
            if(evaluation)
                result.append(element);
        }
        return result;
    }
    PythonStd.filter = filter;


    function reduce(fn,iterable,value=null) {
        if(_.isArray(iterable) ||  _.isMap(iterable))
            if(_.size(iterable) ==0)
                throw  new TypeError("reduce() of empty sequence with no initial value");
        if(iterable instanceof List || iterable instanceof Dict)
            if(iterable.__len__()==0)
            throw  new TypeError("reduce() of empty sequence with no initial value");


        let  iterm =0;
        let result;
        for (let value of iterable) {
            if(iterm==0)
                result = value;
            else
                result = fn(value,result);
            iterm=iterm+1;
        }
        if(value !=null)
            result = fn(result,value);
        return result;
    }
    PythonStd.reduce = reduce;


    class Dict {
        constructor(params) {
            this.map = new Map();
            if (_.isUndefined(params))
                return;
            let kwargs;
            [params, kwargs] = _get_py_params({params}, {}, {kwargs:true}, arguments);
            this.update(params);
            if (kwargs)
                this.update(kwargs);
        }

        __getitem__(key) {
            return this.map.get(key);
        }

        __setitem__(key, value) {
            return this.map.set(key, value);
        }

        __len__() {
            return this.map.size;
        }

        __contains__(key) {
            return this.map.has(key);
        }

        __iter__() {
            return this.map.keys();
        }

        __eq__(other) {
            if (!(other instanceof Dict))
                return false;
            if (this.map.size != other.__len__())
                return false;
            for (let [key, value] of this.map.entries()) {
                if (!_eq(value, _item(other, key)))
                    return False;
            }
            return true;
        }

        [Symbol.iterator]() {
            return this.map.keys();
        }

        keys() {
            return _.map(this.map.keys(), v=>v);
        }

        values() {
            return _.map(this.map.values(), v=>v);
        }

        items() {
            return _.map(this.map.entries(), v=>v);
        }

        entries() {
            return this.map.entries();
        }

        update(data) {
            if (!data)
                throw new TypeError("Empty or wrong parameters to method update");
            if (_.isPlainObject(data)) {
                _.forOwn(data, (value, key) => {this.map.set(key, value)});
            }
            else if (data instanceof Map || data instanceof Dict) {
                for (let [key, value] of data.entries()) {
                    this.map.set(key, value);
                }
            }
            else {
                for (let [key, value] of data) {
                    this.map.set(key, value);
                }
            }
        }

        has(key) {
            return this.map.has(key);
        }

        get(key, defvalue) {
            defvalue = defvalue || null;
            let result = this.map.get(key);
            if (result === undefined)
                return defvalue;
            return result;
        }

        set(key, value) {
            this.map.set(key, value);
        }

        pop(key, defvalue) {
            if (!this.map.has(key)) {
                if (_.isUndefined(defvalue))
                    throw KeyError("The dictionary has no key: "+key);
                return defvalue;
            }
            let result = this.map.get(key);
            this.map.delete(key);
            return result;
        }
    }
    PythonStd.Dict = Dict


    function dict(params) {
        return new Dict(params);
    }
    PythonStd.dict = dict;


    class List {
        constructor(params) {
            this.list = [];
            if (_.isUndefined(params))
                return;
            let kwargs;
            [params, kwargs] = _get_py_params({params}, {}, {kwargs:true}, arguments);
            this.update(params);
            if (kwargs)
                this.update(kwargs);
        }

        [Symbol.iterator]() {
            return this.__iter__();
        }

        __iter__() {
            //yield 444;
            return this.list.values();
        }

        __getitem__(key) {
            if(key<0)
                key=this.list.length+key;
            return this.list[key];
        }

        __setitem__(key, value) {
            return this.list[key]=value;
        }

        __len__() {
            return this.list.length;
        }

        __setslice__(begin,end ,values){
            let object_length = this.list.length;

            if(begin===undefined || begin===null)
                begin=0;
            if(end===undefined || end===null)
                end=object_length;

            let sub_begin=this.list.slice(0,begin);
            let sub_end = this.list.slice(end,this.list.length);
            this.list= [];
            this.list=this.list.concat(sub_begin);
            if (values instanceof Array || values instanceof List) {
                for (let [key,value] of values.entries()) {
                    this.list.append(value);
                }
            }
            else
                this.list=this.list.concat(values);

            this.list=this.list.concat(sub_end);
            return this.list;
        }

        __getslice__(begin, end,step){
            if(begin===undefined || begin===null)
                begin=0;
            if(end===undefined || end===null)
                end=this.list.length;
            if(step===undefined || step===null)
                step=1
            let result = new List([])
            for(let i=begin;i<end;i+=step)
                result.append(this.list[i]);
            return result;
        }

        toString()
        {return this.list.toString();}

        __eq__(other) {
            if (!(other instanceof List)){
                return false;}
            if (this.list.length != other.__len__()) {
                return false;
            }
            for (let i=0;i<this.list.length;i++)
            {
                if (!_eq(this.__getitem__(i),other.__getitem__(i))){
                    return false;
                }
            }
            return true;
        }

        __add__(value){
            this.update(value);
            return this;
        }

        __mul__(value){
            let a_list = this.list;
            for(let i=1;i<value;i++)
               this.list=this.list.concat(a_list);
            return this;
        }

        keys() {
            return this.list.keys();
        }

        values() {
            return this.list.values();
        }

        items() {
            return this.list;
        }

        entries() {
            return this.list.entries();
        }


        update(data) {
            if (!data)
                throw new TypeError("Empty or wrong parameters to method update");
            if (_.isPlainObject(data)) {
                _.forOwn(data, (value) => {this.list.append(value)});
            }
            else
                for (let value of data) {
                    this.list.append( value);
                }
        }



        get(key, defvalue) {
            defvalue = defvalue || null;
            let result = this.list.__getitem__(key);
            if (result === undefined)
                return defvalue;
            return result;
        }

        set(key, value) {
            this.list.__setitem__(key, value);
        }

        pop(indexValue){
            indexValue= indexValue || null;
            if(indexValue!=null && indexValue!=undefined)
                this.list.splice(indexValue,1);
            else
                this.list.pop();
        }


        append(value){
            value= value || null;
            if(_.isUndefined(value))
                throw new TypeError("TypeError: append() takes exactly one argument (0 given)");
            else
                this.list.append(value);

        }

        count(value){
            let count = 0;
            for(let i=0;i<this.list.length;i++)
                if(_eq(this.list[i],value)){
                   count=count+1;
                }
            return count;
        }

        extend(value){
            for (let item of value)
                this.list.append(item);
        }

        index(searchValue,startValue,stopValue){
            startValue= startValue || 0;
            stopValue=stopValue || this.list.length;
            for(let i=startValue;i<stopValue;i++)
                if(_eq(this.list[i],searchValue))
                    return i
            throw new ValueError(searchValue.toString()+" is not in list");
        }

        insert(indexValue,insertValue){
            this.list.splice(indexValue,0,insertValue);
        }
        remove(removeValue){
            for(let i=0;i<this.list.length;i++)
                if(_eq(this.list[i],removeValue)){
                    this.list.splice(i,1);
                    return ;
                }
            return new ValueError("list.remove(x): x not in list");
        }
        reverse(){
            this.list.reverse();
        }
        sort(){
            this.list.sort();
        }
    }
    PythonStd.List = List;


    function list(params) {
        return new List(params);
    }
    PythonStd.list = list;

    if (typeof window === 'undefined')
        module.exports = PythonStd;
    else {
        window.pythonstd = Object.assign({}, PythonStd);
    }
})()
