export class Exception extends Error {
    constructor() {
        super();
        this.arguments = arguments;
    }
}


export class IndexError extends Exception {}

export class KeyError extends Exception {}

export class AttributeError extends Exception {}

export class TypeError extends Exception {}

export class ValueError extends Exception {}

export class ZeroDivisionError extends Exception {}

