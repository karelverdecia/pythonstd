require('babel-polyfill');
import _ from 'lodash';
import { _get_py_params, PyParams} from './parameters';
import { len, _eq, _item } from './operators'


export class List {
    constructor(params) {
        this.list = [];
        if (_.isUndefined(params))
            return;
        let kwargs;
        [params, kwargs] = _get_py_params({params}, {}, {kwargs:true}, arguments);
        this.update(params);
        if (kwargs)
            this.update(kwargs);
    }

    [Symbol.iterator]() {
        return this.__iter__();
    }

    __iter__() {
        //yield 444;
        return this.list.values();
    }

    __getitem__(key) {
        if(key<0)
            key=this.list.length+key;
        return this.list[key];
    }

    __setitem__(key, value) {
        return this.list[key]=value;
    }

    __len__() {
        return this.list.length;
    }

    __setslice__(begin,end ,values){
        let object_length = this.list.length;

        if(begin===undefined || begin===null)
            begin=0;
        if(end===undefined || end===null)
            end=object_length;

        let sub_begin=this.list.slice(0,begin);
        let sub_end = this.list.slice(end,this.list.length);
        this.list= [];
        this.list=this.list.concat(sub_begin);
        if (values instanceof Array || values instanceof List) {
            for (let [key,value] of values.entries()) {
                this.list.append(value);
            }
        }
        else
            this.list=this.list.concat(values);

        this.list=this.list.concat(sub_end);
        return this.list;
    }

    __getslice__(begin, end,step){
        if(begin===undefined || begin===null)
            begin=0;
        if(end===undefined || end===null)
            end=this.list.length;
        if(step===undefined || step===null)
            step=1
        let result = new List([])
        for(let i=begin;i<end;i+=step)
            result.append(this.list[i]);
        return result;
    }

    toString()
    {return this.list.toString();}

    __eq__(other) {
        if (!(other instanceof List)){
            return false;}
        if (this.list.length != other.__len__()) {
            return false;
        }
        for (let i=0;i<this.list.length;i++)
        {
            if (!_eq(this.__getitem__(i),other.__getitem__(i))){
                return false;
            }
        }
        return true;
    }

    __add__(value){
        this.update(value);
        return this;
    }

    __mul__(value){
        let a_list = this.list;
        for(let i=1;i<value;i++)
           this.list=this.list.concat(a_list);
        return this;
    }

    keys() {
        return this.list.keys();
    }

    values() {
        return this.list.values();
    }

    items() {
        return this.list;
    }

    entries() {
        return this.list.entries();
    }


    update(data) {
        if (!data)
            throw new TypeError("Empty or wrong parameters to method update");
        if (_.isPlainObject(data)) {
            _.forOwn(data, (value) => {this.list.append(value)});
        }
        else
            for (let value of data) {
                this.list.append( value);
            }
    }



    get(key, defvalue) {
        defvalue = defvalue || null;
        let result = this.list.__getitem__(key);
        if (result === undefined)
            return defvalue;
        return result;
    }

    set(key, value) {
        this.list.__setitem__(key, value);
    }

    pop(indexValue){
        indexValue= indexValue || null;
        if(indexValue!=null && indexValue!=undefined)
            this.list.splice(indexValue,1);
        else
            this.list.pop();
    }


    append(value){
        value= value || null;
        if(_.isUndefined(value))
            throw new TypeError("TypeError: append() takes exactly one argument (0 given)");
        else
            this.list.append(value);

    }

    count(value){
        let count = 0;
        for(let i=0;i<this.list.length;i++)
            if(_eq(this.list[i],value)){
               count=count+1;
            }
        return count;
    }

    extend(value){
        for (let item of value)
            this.list.append(item);
    }

    index(searchValue,startValue,stopValue){
        startValue= startValue || 0;
        stopValue=stopValue || this.list.length;
        for(let i=startValue;i<stopValue;i++)
            if(_eq(this.list[i],searchValue))
                return i
        throw new ValueError(searchValue.toString()+" is not in list");
    }

    insert(indexValue,insertValue){
        this.list.splice(indexValue,0,insertValue);
    }
    remove(removeValue){
        for(let i=0;i<this.list.length;i++)
            if(_eq(this.list[i],removeValue)){
                this.list.splice(i,1);
                return ;
            }
        return new ValueError("list.remove(x): x not in list");
    }
    reverse(){
        this.list.reverse();
    }
    sort(){
        this.list.sort();
    }
}


export function list(params) {
    return new List(params);
}

