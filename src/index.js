require('babel-polyfill');
import _ from 'lodash'
import * as __exceptions from './exceptions';
import * as __operators from './operators';
import * as __buildins from './buildins';
import * as __dict from './dict';
import * as __list from './list';
import * as __parameters from './parameters';


(function(){
    const PythonSTD = Object.assign({}, __exceptions, __operators, __buildins, __dict, __list, __parameters)
    if (typeof window === 'undefined')
        module.exports = PythonStd;
    else {
        window.pythonstd = Object.assign({}, PythonStd);
    }
})()
