import _  from 'lodash';
import * as parameters from 'pythonstd/lib/parameters'
import * as exceptions from 'pythonstd/lib/exceptions';


const AttributeError= exceptions.AttributeError;
const TypeError = exceptions.TypeError;
const IndexError = exceptions.IndexError;
const KeyError = exceptions.KeyError;
const ZeroDivisionError = exceptions.ZeroDivisionError;


function getattr(value, attribute, defaultValue) {
    if (_.isNull(value) || _.isUndefined(value))
    {
        if (_.isUndefined(defaultValue))
            throw new AttributeError("Null object has no attribute "+attribute);
        return defaultValue;
    }
    var result = value[attribute];
    if (_.isUndefined(result)) {
        if (_.isUndefined(defaultValue))
            throw new AttributeError("Object has no attribute "+attribute);
        return defaultValue;
    }
    return result;
}

export function len(value) {
    value = parameters._get_py_param(value);
    if (_.isArrayLike(value))
        return value.length;
    if (_.isMap(value))
        return value.size;
    if (_.isSet(value))
        return value.size;
    if (_.isFunction(value['__len__']))
        return value['__len__'].bind(value)(value);
    throw new AttributeError("Object has no method __len__.");
}

export function _eq(v1, v2) {
    var __eq__ = getattr(v1, '__eq__', null);
    if (_.isFunction(__eq__))
        return __eq__.bind(v1)(v2);
    var __eq__ = getattr(v2, '__eq__', null);
    if (_.isFunction(__eq__))
        return __eq__.bind(v2)(v1);
    return v1 === v2;
}


export function _ne(v1, v2) {
    return !_eq(v1, v2);
}

String.prototype.__len__ = function() {
    return this.length;
};


String.prototype.__getitem__ = function(key) {
    if (!_.isInteger(key))
        throw new TypeError("Array keys must be integers.", key);
    var _key = key;
    if (_key < 0)
        _key += this.length;
    if (_key < 0 || _key>=this.length)
        throw new IndexError("Index error", key);
    return this[_key];
};


String.prototype.__contains__ = function(value) {
    return this.indexOf(value) > -1;
};


String.prototype.__str__ = function() {
    return this;
};


String.prototype.__unicode__ = function() {
    return this;
};


Array.prototype.__len__ = function() {
    return this.length;
};


Array.prototype.__getitem__ = function(key) {
    if (!_.isInteger(key))
        throw new TypeError("Array keys must be integers.", key);
    var _key = key;
    if (_key < 0)
        _key += this.length;
    if (_key < 0 || _key>=this.length)
        throw new IndexError("Index error", key);
    return this[_key];
};


Array.prototype.__setitem__ = function(key, value) {
    if (!_.isInteger(key))
        throw new TypeError("Array keys must be integers.", key);
    this[key] = value;
};


Array.prototype.__contains__ = function(value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === value)
            return true;
    }
    return false;
};


Array.prototype.__eq__ = function(value) {
    if (value instanceof Array) {
        if (this.length != value.length)
            return false;
        for (var i = 0; i < this.length; i++) {
            if (!_eq(this[i], value[i]))
                return false;
        }
        return true;
    }
    return false;
};


Array.prototype.__add__ = function(value) {
    if (value instanceof Array) {
        var result = [];
        _.forEach(this, function(v) {result.push(v)});
        _.forEach(value, function(v) {result.push(v)});
        return result;
    }
    throw new TypeError("Cannot concat an array with a value of other type.");
};

Array.prototype.append = function(value) {
    value = parameters._get_py_param(value);
    this.push(value);
    return this;
};

Array.prototype.extend = function(value) {
    value = parameters._get_py_param(value);
    if (value instanceof Array) {
        Array.prototype.push.apply(this,value);
    }
    else{
        throw new TypeError("TypeError: "+ type_value +" object is not iterable");
    }

};

Array.prototype.__mul__ = function (value) {
    if (_.isInteger(value)) {
        var result = [];
        for (var i = 0; i < value; i++) {
            _.forEach(this, function(v) { result.push(v); });
        }
        return result;
    }
    throw new TypeError("Cannot multiply an array by a non integer valuer");
};


Map.prototype.__len__ = function() {
    return this.size;
};


Map.prototype.__getitem__ = function(key) {
    var result = this.get(key);
    if (_.isUndefined(result))
        throw new KeyError("Object has no key", index);
    return result;
};


Map.prototype.__setitem__ = function(key, value) {
    this.set(key, value);
};


Map.prototype.__delitem__ = function (key) {
    var result = this.delete(key);
    if (!result)
        throw new KeyError("Dictionary has no key", key);
    this.delete(key);
};


Map.prototype.__contains__ = function(key) {
    return this.has(key);
};


Set.prototype.__len__ = function() {
    return this.size;
};


Set.prototype.__contains__ = function(value) {
    return this.has(value);
};


export function _item(value, index) {
    var __getitem__ = getattr(value, '__getitem__', null);
    if (_.isFunction(__getitem__))
        return __getitem__.bind(value)(index);
    var result = value[index];
    if (_.isUndefined(result))
        throw new KeyError("Object has no key "+index);
    return result;
}

export function _setitem(array_object,index, value){
    array_object.__setitem__(index,value);
}

export function _div(value1, value2, isInt) {
    if(_.isNumber(value1) && _.isNumber(value2))
        if(value2!=0)
        {
            if (isInt === undefined){return value1 / value2;}
            else {
                var result = value1/value2;
                if(result<0)
                    result=result-1;
                return Number(result.toFixed(0));
            }

        }
        else
            throw new ZeroDivisionError("Integer division or modulo by zero")
    else{
        var type_v1=  typeof(value1);
        var type_v2=  typeof(value2);
        throw new TypeError("Unsupported operand type(s) for /:", type_v1+" and " +type_v2);
    }
}

export function _setslice(objectList,begin,end,values){
   return  objectList.__setslice__(begin,end,values);
}

export function _slice(objectList,start, stop, step) {
    if(_.isString(objectList)){
        var result = _.slice(objectList, start, stop, step);
        if (_.isString(objectList))
            return _.join(result, '');
        return result;
    }
    else{
        return objectList.__getslice__(start,stop,step);
    }
}


export function _add(value1, value2) {
    var __add__ = getattr(value1, '__add__', null);
    if (_.isFunction(__add__))
        return __add__.bind(value1)(value2);
    return value1 + value2;
}


export function _sub(value1, value2) {
    return value1 - value2;
}


export function _mul(value1, value2) {
    var __mul__ = getattr(value1, '__mul__', null);
    if (_.isFunction(__mul__))
        return __mul__.bind(value1)(value2);
    var __mul__ = getattr(value2, '__mul__', null);
    if (_.isFunction(__mul__))
        return __mul__.bind(value2)(value1);

    var text = null;
    var count = null;
    if (_.isString(value1) && _.isInteger(value2)) {
        text = value1;
        count = value2;
    }
    if (_.isInteger(value1) && _.isString(value2)) {
        text = value2;
        count = value1;
    }
    if (!_.isNull(text)) {
        return _.repeat(text, count);
    }
    return value1 * value2;
}


export function _mod(value1, value2) {
    return value1 % value2;
}


export function _isTrue(value) {
    if (value === 0)
        return false;
    if (value === "")
        return false;
    if (value === false)
        return false;
    if (value === null)
        return false;
    if (value === undefined)
        return false;
    if (value instanceof Array || _.isPlainObject(value) || value instanceof Set || value instanceof Map)
        return !_.isEmpty(value);
    if (_.isFunction(value['__nonzero__']))
        return value['__nonzero__'].bind(value)();
    if (_.isFunction(value['__len__']))
        return value['__len__'].bind(value)();
    return true;
}


export function _not(value) {
    return !_isTrue(value);
}


export function _lt(value1, value2) {
    try{
        return value1<value2;
    }
    catch(e)
    {
        var type_v1=  typeof(value1);
        var type_v2=  typeof(value2);
        throw new TypeError("Unsupported operand type(s) for <", type_v1+" and " +type_v2);
    }
}

export function _gt(value1, value2) {
    try{
        return value1>value2;
    }
    catch(e)
    {
        var type_v1=  typeof(value1);
        var type_v2=  typeof(value2);
        throw new TypeError("Unsupported operand type(s) for >:", type_v1+" and " +type_v2);
    }
}

export function _lte(value1, value2) {
    try{
        return value1<=value2;
    }
    catch(e)
    {
        var type_v1=  typeof(value1);
        var type_v2=  typeof(value2);
        throw new TypeError("Unsupported operand type(s) for <=:", type_v1+" and " +type_v2);
    }
}

export function _gte(value1, value2) {
    try{
        return value1>=value2 ;
    }
    catch(e)
    {
        var type_v1=  typeof(value1);
        var type_v2=  typeof(value2);
        throw new TypeError("Unsupported operand type(s) for >=:", type_v1+" and " +type_v2);
    }
}


export function _in(value, obj) {
    if (obj instanceof Set || obj instanceof Map)
        return obj.has(value);
    if (obj instanceof Array) {
        for (let item of obj) {
            if (_eq(item, value))
                return true;
        }
        return false;
    }
    if (obj['__contains__'])
        return obj['__contains__'](value);
    return false;
}

