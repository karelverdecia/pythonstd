import _ from 'lodash';
import { _get_py_params } from './parameters';
import { len } from './buildins';
import { _eq, _item } from './operators'


export class Dict {
    constructor(params) {
        this.map = new Map();
        if (_.isUndefined(params))
            return;
        let kwargs;
        [params, kwargs] = _get_py_params({params}, {}, {kwargs:true}, arguments);
        this.update(params);
        if (kwargs)
            this.update(kwargs);
    }

    __getitem__(key) {
        return this.map.get(key);
    }

    __setitem__(key, value) {
        return this.map.set(key, value);
    }

    __len__() {
        return this.map.size;
    }

    __contains__(key) {
        return this.map.has(key);
    }

    __iter__() {
        return this.map.keys();
    }

    __eq__(other) {
        if (!(other instanceof Dict))
            return false;
        if (this.map.size != other.__len__())
            return false;
        for (let [key, value] of this.map.entries()) {
            if (!_eq(value, _item(other, key)))
                return False;
        }
        return true;
    }

    [Symbol.iterator]() {
        return this.map.keys();
    }

    keys() {
        return _.map(this.map.keys(), v=>v);
    }

    values() {
        return _.map(this.map.values(), v=>v);
    }

    items() {
        return _.map(this.map.entries(), v=>v);
    }

    entries() {
        return this.map.entries();
    }

    update(data) {
        if (!data)
            throw new TypeError("Empty or wrong parameters to method update");
        if (_.isPlainObject(data)) {
            _.forOwn(data, (value, key) => {this.map.set(key, value)});
        }
        else if (data instanceof Map || data instanceof Dict) {
            for (let [key, value] of data.entries()) {
                this.map.set(key, value);
            }
        }
        else {
            for (let [key, value] of data) {
                this.map.set(key, value);
            }
        }
    }

    has(key) {
        return this.map.has(key);
    }

    get(key, defvalue) {
        defvalue = defvalue || null;
        let result = this.map.get(key);
        if (result === undefined)
            return defvalue;
        return result;
    }

    set(key, value) {
        this.map.set(key, value);
    }

    pop(key, defvalue) {
        if (!this.map.has(key)) {
            if (_.isUndefined(defvalue))
                throw KeyError("The dictionary has no key: "+key);
            return defvalue;
        }
        let result = this.map.get(key);
        this.map.delete(key);
        return result;
    }
}


export function dict(params) {
    return new Dict(params);
}
