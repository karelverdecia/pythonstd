import _              from 'lodash';
import { TypeError }  from './exceptions';
import { dict, Dict } from './dict';
import { List }       from  './list';

export class PyParams {
    constructor(data) {
        let params = data.params || [];
        if (params instanceof List)
            params = params.list;
        let args = data.args || [];
        if (args instanceof List)
            args = args.items();
        this.args = _.concat(params, args);
        let keys = data.keys || {};
        let kwargs = data.kwargs || {};
        if (!_.isPlainObject(keys) && !(keys instanceof Dict))
            throw new TypeError("PyParam key must be a plain object or a PyDict.");
        if (!_.isPlainObject(kwargs) && !(kwargs instanceof Dict))
            throw new TypeError("PyParam kwargs must be a plain object or a PyDict.");
        if (!(keys instanceof Dict))
            keys = dict(keys);
        if (!(kwargs instanceof Dict))
            kwargs = dict(kwargs);
        for (let key of keys) {
            if (kwargs.has(key))
                throw new TypeError("Function got multiple values for keyword argument "+key);
        }
        this.kwargs = keys;
        this.kwargs.update(kwargs);
    }

    pop(name, defvalue) {
        var result;
        if (this.args.length) {
            if (this.kwargs.has(name))
                throw new TypeError("Function got multiple values for keyword argument"+name);
            result = this.args[0];
            this.args = _.drop(this.args);
            return result
        }
        if (!this.kwargs.has(name)) {
            if (_.isUndefined(defvalue))
                throw new TypeError("Missing value for argument " + name);
            return defvalue;
        }
        return this.kwargs.pop(name);
    }

    hasKeyword(name) {
        return !_.isUndefined(this.kwargs[name]);
    }

    first() {
        return this.args[0];
    }

    getVarArgs() {
        return this.args;
    }

    getKwArgs() {
        return this.kwargs;
    }
}


export function _py_params(data) {
    return new PyParams(data);
}

export function _get_py_param(value) {
    if (value instanceof PyParams)
        return value.first();
    return value;
}

export function _get_py_params(params, defaults, extra, _arguments) {
    if (!_.isPlainObject(params))
        throw new TypeError("Wrong parameters for _get_simple_params function.");
    if (!_.isPlainObject(defaults))
        throw new TypeError("Wrong parameters for _get_simple_params function.");
    if (!_.isPlainObject(extra))
        throw new TypeError("Wrong parameters for _get_simple_params function.");
    extra = extra || {};
    let result = [];
    if (_arguments[0] instanceof PyParams) {
        let pyParams = _arguments[0];
        _.forOwn(params, (value, key) => {
            result.push(pyParams.pop(key, defaults[key]));
        });
        if (extra.args) {
            result.push(pyParams.getVarArgs());
        }
        if (extra.kwargs) {
            result.push(pyParams.getKwArgs());
        }
    }
    else {
        _.forOwn(params, (value, key) => {
            if (_.isUndefined(value)) {
                if (_.isUndefined(defaults[key]))
                    throw new TypeError("The function has received not enough arguments.");
                value = defaults[key];
            }
            result.push(value);
        });
        if (extra.args) {
            let args = [];
            for (let i = result.length; i < _arguments.length; i++) {
                args.push(_arguments[i]);
            }
            result.push(args);
        }
    }
    return result;
}

