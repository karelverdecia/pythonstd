var path    = require('path')
    ,   webpack = require('webpack')
    ;

module.exports = {
    entry: [
        'babel-polyfill',
        './src' // Your appʼs entry point
    ],
    output: {
        path: path.join(__dirname, 'lib'),
        filename: "python-std.js"
    },
    plugins: [
        //new webpack.optimize.OccurenceOrderPlugin(),
        //new webpack.optimize.UglifyJsPlugin()
    ],
    resolve: {
        extensions: ['', '.js', '.json', '.jsx', '.less'],
        /*alias: {
            'appRoot': path.join(__dirname, 'js/protek-dashboards'),
            'vendor': 'appRoot/vendor'
        }*/
    },
    module: {
        loaders: [
            { test: /\.less$/,      loader: 'style-loader!css-loader!postcss-loader!less-loader' },
            { test: /\.css$/,       loader: 'style-loader!css-loader' },
            { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'}, // inline base64 URLs for <=8k images, direct URLs for the rest
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"]
            },

            { test: /\.(woff|woff2)$/, loader: "url?limit=10000&minetype=application/font-woff&name=/static/fonts/[name].[ext]" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml&name=/static/fonts/[name].[ext]'},
            {
                test: /\.ttf$|\.eot$/,
                loaders: ['file?prefix=font/&name=/static/fonts/[name].[ext]']
            },
            {
                test: /\.jsx?$/,
                include: [
                    path.join(__dirname, 'js'), // files to apply this loader to
                ],
                // http://jamesknelson.com/using-es6-in-the-browser-with-babel-6-and-webpack/
                loaders: ['babel?presets[]=react,presets[]=es2015'] // loaders process from right to left
            }
        ]
    }
};
