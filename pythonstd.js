'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

require('babel-polyfill');


(function () {
    var _marked = [izip_max].map(regeneratorRuntime.mark);

    var PythonStd = {};

    var Exception = function (_Error) {
        _inherits(Exception, _Error);

        function Exception() {
            _classCallCheck(this, Exception);

            var _this = _possibleConstructorReturn(this, (Exception.__proto__ || Object.getPrototypeOf(Exception)).call(this));

            _this.arguments = arguments;
            return _this;
        }

        return Exception;
    }(Error);

    PythonStd.Exception = Exception;

    var IndexError = function (_Exception) {
        _inherits(IndexError, _Exception);

        function IndexError() {
            _classCallCheck(this, IndexError);

            return _possibleConstructorReturn(this, (IndexError.__proto__ || Object.getPrototypeOf(IndexError)).apply(this, arguments));
        }

        return IndexError;
    }(Exception);

    PythonStd.IndexError = IndexError;

    var KeyError = function (_Exception2) {
        _inherits(KeyError, _Exception2);

        function KeyError() {
            _classCallCheck(this, KeyError);

            return _possibleConstructorReturn(this, (KeyError.__proto__ || Object.getPrototypeOf(KeyError)).apply(this, arguments));
        }

        return KeyError;
    }(Exception);

    PythonStd.KeyError = KeyError;

    var AttributeError = function (_Exception3) {
        _inherits(AttributeError, _Exception3);

        function AttributeError() {
            _classCallCheck(this, AttributeError);

            return _possibleConstructorReturn(this, (AttributeError.__proto__ || Object.getPrototypeOf(AttributeError)).apply(this, arguments));
        }

        return AttributeError;
    }(Exception);

    PythonStd.AttributeError = AttributeError;

    var TypeError = function (_Exception4) {
        _inherits(TypeError, _Exception4);

        function TypeError() {
            _classCallCheck(this, TypeError);

            return _possibleConstructorReturn(this, (TypeError.__proto__ || Object.getPrototypeOf(TypeError)).apply(this, arguments));
        }

        return TypeError;
    }(Exception);

    PythonStd.TypeError = TypeError;

    var ValueError = function (_Exception5) {
        _inherits(ValueError, _Exception5);

        function ValueError() {
            _classCallCheck(this, ValueError);

            return _possibleConstructorReturn(this, (ValueError.__proto__ || Object.getPrototypeOf(ValueError)).apply(this, arguments));
        }

        return ValueError;
    }(Exception);

    PythonStd.ValueError = ValueError;

    var ZeroDivisionError = function (_Exception6) {
        _inherits(ZeroDivisionError, _Exception6);

        function ZeroDivisionError() {
            _classCallCheck(this, ZeroDivisionError);

            return _possibleConstructorReturn(this, (ZeroDivisionError.__proto__ || Object.getPrototypeOf(ZeroDivisionError)).apply(this, arguments));
        }

        return ZeroDivisionError;
    }(Exception);

    PythonStd.ZeroDivisionError = ZeroDivisionError;

    var PyParams = function () {
        function PyParams(data) {
            _classCallCheck(this, PyParams);

            var params = data.params || [];
            if (params instanceof List) params = params.list;
            var args = data.args || [];
            if (args instanceof List) args = args.items();
            this.args = _lodash2.default.concat(params, args);
            var keys = data.keys || {};
            var kwargs = data.kwargs || {};
            if (!_lodash2.default.isPlainObject(keys) && !(keys instanceof Dict)) throw new TypeError("PyParam key must be a plain object or a PyDict.");
            if (!_lodash2.default.isPlainObject(kwargs) && !(kwargs instanceof Dict)) throw new TypeError("PyParam kwargs must be a plain object or a PyDict.");
            if (!(keys instanceof Dict)) keys = dict(keys);
            if (!(kwargs instanceof Dict)) kwargs = dict(kwargs);
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = keys[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var key = _step.value;

                    if (kwargs.has(key)) throw new TypeError("Function got multiple values for keyword argument " + key);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            this.kwargs = keys;
            this.kwargs.update(kwargs);
        }

        _createClass(PyParams, [{
            key: 'pop',
            value: function pop(name, defvalue) {
                var result;
                if (this.args.length) {
                    if (this.kwargs.has(name)) throw new TypeError("Function got multiple values for keyword argument" + name);
                    result = this.args[0];
                    this.args = _lodash2.default.drop(this.args);
                    return result;
                }
                if (!this.kwargs.has(name)) {
                    if (_lodash2.default.isUndefined(defvalue)) throw new TypeError("Missing value for argument " + name);
                    return defvalue;
                }
                return this.kwargs.pop(name);
            }
        }, {
            key: 'hasKeyword',
            value: function hasKeyword(name) {
                return !_lodash2.default.isUndefined(this.kwargs[name]);
            }
        }, {
            key: 'first',
            value: function first() {
                return this.args[0];
            }
        }, {
            key: 'getVarArgs',
            value: function getVarArgs() {
                return this.args;
            }
        }, {
            key: 'getKwArgs',
            value: function getKwArgs() {
                return this.kwargs;
            }
        }]);

        return PyParams;
    }();

    PythonStd.PyParams = PyParams;

    function _py_params(data) {
        return new PyParams(data);
    }
    PythonStd._py_params = _py_params;

    function _get_py_param(value) {
        if (value instanceof PyParams) return value.first();
        return value;
    }
    PythonStd._get_py_param = _get_py_param;

    function _get_py_params(params, defaults, extra, _arguments) {
        if (!_lodash2.default.isPlainObject(params)) throw new TypeError("Wrong parameters for _get_simple_params function.");
        if (!_lodash2.default.isPlainObject(defaults)) throw new TypeError("Wrong parameters for _get_simple_params function.");
        if (!_lodash2.default.isPlainObject(extra)) throw new TypeError("Wrong parameters for _get_simple_params function.");
        extra = extra || {};
        var result = [];
        if (_arguments[0] instanceof PyParams) {
            (function () {
                var pyParams = _arguments[0];
                _lodash2.default.forOwn(params, function (value, key) {
                    result.push(pyParams.pop(key, defaults[key]));
                });
                if (extra.args) {
                    result.push(pyParams.getVarArgs());
                }
                if (extra.kwargs) {
                    result.push(pyParams.getKwArgs());
                }
            })();
        } else {
            _lodash2.default.forOwn(params, function (value, key) {
                if (_lodash2.default.isUndefined(value)) {
                    if (_lodash2.default.isUndefined(defaults[key])) throw new TypeError("The function has received not enough arguments.");
                    value = defaults[key];
                }
                result.push(value);
            });
            if (extra.args) {
                var args = [];
                for (var i = result.length; i < _arguments.length; i++) {
                    args.push(_arguments[i]);
                }
                result.push(args);
            }
        }
        return result;
    }
    PythonStd._get_py_params = _get_py_params;

    function getattr(value, attribute, defaultValue) {
        if (_lodash2.default.isNull(value) || _lodash2.default.isUndefined(value)) {
            if (_lodash2.default.isUndefined(defaultValue)) throw new AttributeError("Null object has no attribute " + attribute);
            return defaultValue;
        }
        var result = value[attribute];
        if (_lodash2.default.isUndefined(result)) {
            if (_lodash2.default.isUndefined(defaultValue)) throw new AttributeError("Object has no attribute " + attribute);
            return defaultValue;
        }
        return result;
    }
    PythonStd.getattr = getattr;

    function len(value) {
        value = _get_py_param(value);
        if (_lodash2.default.isArrayLike(value)) return value.length;
        if (_lodash2.default.isMap(value)) return value.size;
        if (_lodash2.default.isSet(value)) return value.size;
        if (_lodash2.default.isFunction(value['__len__'])) return value['__len__'].bind(value)(value);
        throw new AttributeError("Object has no method __len__.");
    }
    PythonStd.len = len;

    function _eq(v1, v2) {
        var __eq__ = getattr(v1, '__eq__', null);
        if (_lodash2.default.isFunction(__eq__)) return __eq__.bind(v1)(v2);
        var __eq__ = getattr(v2, '__eq__', null);
        if (_lodash2.default.isFunction(__eq__)) return __eq__.bind(v2)(v1);
        return v1 === v2;
    }
    PythonStd._eq = _eq;

    function _ne(v1, v2) {
        return !_eq(v1, v2);
    }
    PythonStd._ne = _ne;

    String.prototype.__len__ = function () {
        return this.length;
    };

    String.prototype.__getitem__ = function (key) {
        if (!_lodash2.default.isInteger(key)) throw new TypeError("Array keys must be integers.", key);
        var _key = key;
        if (_key < 0) _key += this.length;
        if (_key < 0 || _key >= this.length) throw new IndexError("Index error", key);
        return this[_key];
    };

    String.prototype.__contains__ = function (value) {
        return this.indexOf(value) > -1;
    };

    String.prototype.__str__ = function () {
        return this;
    };

    String.prototype.__unicode__ = function () {
        return this;
    };

    Array.prototype.__len__ = function () {
        return this.length;
    };

    Array.prototype.__getitem__ = function (key) {
        if (!_lodash2.default.isInteger(key)) throw new TypeError("Array keys must be integers.", key);
        var _key = key;
        if (_key < 0) _key += this.length;
        if (_key < 0 || _key >= this.length) throw new IndexError("Index error", key);
        return this[_key];
    };

    Array.prototype.__setitem__ = function (key, value) {
        if (!_lodash2.default.isInteger(key)) throw new TypeError("Array keys must be integers.", key);
        this[key] = value;
    };

    Array.prototype.__contains__ = function (value) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === value) return true;
        }
        return false;
    };

    Array.prototype.__eq__ = function (value) {
        if (value instanceof Array) {
            if (this.length != value.length) return false;
            for (var i = 0; i < this.length; i++) {
                if (!_eq(this[i], value[i])) return false;
            }
            return true;
        }
        return false;
    };

    Array.prototype.__add__ = function (value) {
        if (value instanceof Array) {
            var result = [];
            _lodash2.default.forEach(this, function (v) {
                result.push(v);
            });
            _lodash2.default.forEach(value, function (v) {
                result.push(v);
            });
            return result;
        }
        throw new TypeError("Cannot concat an array with a value of other type.");
    };

    Array.prototype.append = function (value) {
        value = _get_py_param(value);
        this.push(value);
        return this;
    };

    Array.prototype.extend = function (value) {
        value = _get_py_param(value);
        if (value instanceof Array) {
            Array.prototype.push.apply(this, value);
        } else {
            throw new TypeError("TypeError: " + type_value + " object is not iterable");
        }
    };

    Array.prototype.__mul__ = function (value) {
        if (_lodash2.default.isInteger(value)) {
            var result = [];
            for (var i = 0; i < value; i++) {
                _lodash2.default.forEach(this, function (v) {
                    result.push(v);
                });
            }
            return result;
        }
        throw new TypeError("Cannot multiply an array by a non integer valuer");
    };

    Map.prototype.__len__ = function () {
        return this.size;
    };

    Map.prototype.__getitem__ = function (key) {
        var result = this.get(key);
        if (_lodash2.default.isUndefined(result)) throw new KeyError("Object has no key", index);
        return result;
    };

    Map.prototype.__setitem__ = function (key, value) {
        this.set(key, value);
    };

    Map.prototype.__delitem__ = function (key) {
        var result = this.delete(key);
        if (!result) throw new KeyError("Dictionary has no key", key);
        this.delete(key);
    };

    Map.prototype.__contains__ = function (key) {
        return this.has(key);
    };

    Set.prototype.__len__ = function () {
        return this.size;
    };

    Set.prototype.__contains__ = function (value) {
        return this.has(value);
    };

    function _item(value, index) {
        var __getitem__ = getattr(value, '__getitem__', null);
        if (_lodash2.default.isFunction(__getitem__)) return __getitem__.bind(value)(index);
        var result = value[index];
        if (_lodash2.default.isUndefined(result)) throw new KeyError("Object has no key " + index);
        return result;
    }
    PythonStd._item = _item;

    function _setitem(array_object, index, value) {
        array_object.__setitem__(index, value);
    }
    PythonStd._setitem = _setitem;

    function _div(value1, value2, isInt) {
        if (_lodash2.default.isNumber(value1) && _lodash2.default.isNumber(value2)) {
            if (value2 != 0) {
                if (isInt === undefined) {
                    return value1 / value2;
                } else {
                    var result = value1 / value2;
                    if (result < 0) result = result - 1;
                    return Number(result.toFixed(0));
                }
            } else throw new ZeroDivisionError("Integer division or modulo by zero");
        } else {
            var type_v1 = typeof value1 === 'undefined' ? 'undefined' : _typeof(value1);
            var type_v2 = typeof value2 === 'undefined' ? 'undefined' : _typeof(value2);
            throw new TypeError("Unsupported operand type(s) for /:", type_v1 + " and " + type_v2);
        }
    }
    PythonStd._div = _div;

    function _setslice(objectList, begin, end, values) {
        return objectList.__setslice__(begin, end, values);
    }
    PythonStd._setslice = _setslice;

    function _slice(objectList, start, stop, step) {
        if (_lodash2.default.isString(objectList)) {
            var result = _lodash2.default.slice(objectList, start, stop, step);
            if (_lodash2.default.isString(objectList)) return _lodash2.default.join(result, '');
            return result;
        } else {
            return objectList.__getslice__(start, stop, step);
        }
    }
    PythonStd._slice = _slice;

    function _add(value1, value2) {
        var __add__ = getattr(value1, '__add__', null);
        if (_lodash2.default.isFunction(__add__)) return __add__.bind(value1)(value2);
        return value1 + value2;
    }
    PythonStd._add = _add;

    function _sub(value1, value2) {
        return value1 - value2;
    }
    PythonStd._sub = _sub;

    function _mul(value1, value2) {
        var __mul__ = getattr(value1, '__mul__', null);
        if (_lodash2.default.isFunction(__mul__)) return __mul__.bind(value1)(value2);
        var __mul__ = getattr(value2, '__mul__', null);
        if (_lodash2.default.isFunction(__mul__)) return __mul__.bind(value2)(value1);

        var text = null;
        var count = null;
        if (_lodash2.default.isString(value1) && _lodash2.default.isInteger(value2)) {
            text = value1;
            count = value2;
        }
        if (_lodash2.default.isInteger(value1) && _lodash2.default.isString(value2)) {
            text = value2;
            count = value1;
        }
        if (!_lodash2.default.isNull(text)) {
            return _lodash2.default.repeat(text, count);
        }
        return value1 * value2;
    }
    PythonStd._mul = _mul;

    function _mod(value1, value2) {
        return value1 % value2;
    }
    PythonStd._mod = _mod;

    function _isTrue(value) {
        if (value === 0) return false;
        if (value === "") return false;
        if (value === false) return false;
        if (value === null) return false;
        if (value === undefined) return false;
        if (value instanceof Array || _lodash2.default.isPlainObject(value) || value instanceof Set || value instanceof Map) return !_lodash2.default.isEmpty(value);
        if (_lodash2.default.isFunction(value['__nonzero__'])) return value['__nonzero__'].bind(value)();
        if (_lodash2.default.isFunction(value['__len__'])) return value['__len__'].bind(value)();
        return true;
    }
    PythonStd._isTrue = _isTrue;

    function _not(value) {
        return !_isTrue(value);
    }
    PythonStd._not = _not;

    function _lt(value1, value2) {
        try {
            return value1 < value2;
        } catch (e) {
            var type_v1 = typeof value1 === 'undefined' ? 'undefined' : _typeof(value1);
            var type_v2 = typeof value2 === 'undefined' ? 'undefined' : _typeof(value2);
            throw new TypeError("Unsupported operand type(s) for <", type_v1 + " and " + type_v2);
        }
    }
    PythonStd._lt = _lt;

    function _gt(value1, value2) {
        try {
            return value1 > value2;
        } catch (e) {
            var type_v1 = typeof value1 === 'undefined' ? 'undefined' : _typeof(value1);
            var type_v2 = typeof value2 === 'undefined' ? 'undefined' : _typeof(value2);
            throw new TypeError("Unsupported operand type(s) for >:", type_v1 + " and " + type_v2);
        }
    }
    PythonStd._gt = _gt;

    function _lte(value1, value2) {
        try {
            return value1 <= value2;
        } catch (e) {
            var type_v1 = typeof value1 === 'undefined' ? 'undefined' : _typeof(value1);
            var type_v2 = typeof value2 === 'undefined' ? 'undefined' : _typeof(value2);
            throw new TypeError("Unsupported operand type(s) for <=:", type_v1 + " and " + type_v2);
        }
    }
    PythonStd._lte = _lte;

    function _gte(value1, value2) {
        try {
            return value1 >= value2;
        } catch (e) {
            var type_v1 = typeof value1 === 'undefined' ? 'undefined' : _typeof(value1);
            var type_v2 = typeof value2 === 'undefined' ? 'undefined' : _typeof(value2);
            throw new TypeError("Unsupported operand type(s) for >=:", type_v1 + " and " + type_v2);
        }
    }
    PythonStd._gte = _gte;

    function _in(value, obj) {
        if (obj instanceof Set || obj instanceof Map) return obj.has(value);
        if (obj instanceof Array) {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = obj[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var item = _step2.value;

                    if (_eq(item, value)) return true;
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            return false;
        }
        if (obj['__contains__']) return obj['__contains__'](value);
        return false;
    }
    PythonStd._in = _in;

    function int(value) {
        value = _get_py_param(value);
        return _lodash2.default.toInteger(value);
    }
    PythonStd.int = int;

    function float(value) {
        value = _get_py_param(value);
        return _lodash2.default.toNumber(value);
    }
    PythonStd.float = float;

    function str(value) {
        value = _get_py_param(value);
        if (_lodash2.default.isObject(value)) {
            var __str__ = getattr(value, '__str__');
            if (_lodash2.default.isFunction(__str__)) return __str__.bind(value)();
        }
        return _lodash2.default.toString(value);
    }
    PythonStd.str = str;

    function unicode(value) {
        value = _get_py_param(value);
        if (_lodash2.default.isObject(value)) {
            var __unicode__ = getattr(value, '__unicode__');
            if (_lodash2.default.isFunction(__unicode__)) return __unicode__.bind(value)();
        }
        return str(value);
    }
    PythonStd.unicode = unicode;

    function println(value) {
        value = _get_py_param(value);
        if (value === false) console.error(colors.red(value));else console.log(value);
        console.log('\n');
    }
    PythonStd.println = println;

    function abs(value) {
        value = _get_py_param(value);
        return Math.abs(value);
    }
    PythonStd.abs = abs;

    function hasattr(value, attribute) {
        value = _get_py_param(value);
        var attrValue = value[attribute];
        return !_lodash2.default.isUndefined(attrValue);
    }
    PythonStd.hasattr = hasattr;

    function getattr(value, attribute, defaultValue) {
        if (_lodash2.default.isNull(value) || _lodash2.default.isUndefined(value)) {
            if (_lodash2.default.isUndefined(defaultValue)) throw new AttributeError("Null object has no attribute " + attribute);
            return defaultValue;
        }
        var result = value[attribute];
        if (_lodash2.default.isUndefined(result)) {
            if (_lodash2.default.isUndefined(defaultValue)) throw new AttributeError("Object has no attribute " + attribute);
            return defaultValue;
        }
        return result;
    }
    PythonStd.getattr = getattr;

    function setattr(obj, attr, value) {
        obj[attr] = value;
    }
    PythonStd.setattr = setattr;

    function open(file, encoding) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var text = reader.result;
        };
        reader.readAsText(file, encoding);
    }
    PythonStd.open = open;

    function izip_max() {
        var tmp,
            iterators,
            _iteratorNormalCompletion3,
            _didIteratorError3,
            _iteratorError3,
            _iterator3,
            _step3,
            item,
            hasNext,
            result,
            _iteratorNormalCompletion4,
            _didIteratorError4,
            _iteratorError4,
            _iterator4,
            _step4,
            it,
            value,
            _args = arguments;

        return regeneratorRuntime.wrap(function izip_max$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        tmp = Array.from(_args);

                        tmp.shift();
                        iterators = [];
                        _iteratorNormalCompletion3 = true;
                        _didIteratorError3 = false;
                        _iteratorError3 = undefined;
                        _context.prev = 6;

                        for (_iterator3 = _args[Symbol.iterator](); !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            item = _step3.value;

                            iterators.push(item[Symbol.iterator]());
                        }
                        _context.next = 14;
                        break;

                    case 10:
                        _context.prev = 10;
                        _context.t0 = _context['catch'](6);
                        _didIteratorError3 = true;
                        _iteratorError3 = _context.t0;

                    case 14:
                        _context.prev = 14;
                        _context.prev = 15;

                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }

                    case 17:
                        _context.prev = 17;

                        if (!_didIteratorError3) {
                            _context.next = 20;
                            break;
                        }

                        throw _iteratorError3;

                    case 20:
                        return _context.finish(17);

                    case 21:
                        return _context.finish(14);

                    case 22:
                        hasNext = true;

                    case 23:
                        if (!hasNext) {
                            _context.next = 50;
                            break;
                        }

                        hasNext = false;
                        result = [];
                        _iteratorNormalCompletion4 = true;
                        _didIteratorError4 = false;
                        _iteratorError4 = undefined;
                        _context.prev = 29;

                        for (_iterator4 = iterators[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                            it = _step4.value;
                            value = it.next();

                            if (!value.done) {
                                hasNext = true;
                                result.push(value.value);
                            } else result.push(null);
                        }
                        _context.next = 37;
                        break;

                    case 33:
                        _context.prev = 33;
                        _context.t1 = _context['catch'](29);
                        _didIteratorError4 = true;
                        _iteratorError4 = _context.t1;

                    case 37:
                        _context.prev = 37;
                        _context.prev = 38;

                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }

                    case 40:
                        _context.prev = 40;

                        if (!_didIteratorError4) {
                            _context.next = 43;
                            break;
                        }

                        throw _iteratorError4;

                    case 43:
                        return _context.finish(40);

                    case 44:
                        return _context.finish(37);

                    case 45:
                        if (!hasNext) {
                            _context.next = 48;
                            break;
                        }

                        _context.next = 48;
                        return result;

                    case 48:
                        _context.next = 23;
                        break;

                    case 50:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _marked[0], this, [[6, 10, 14, 22], [15,, 17, 21], [29, 33, 37, 45], [38,, 40, 44]]);
    }
    PythonStd.izip_max = izip_max;

    function map(fn) {
        var args = void 0;

        var _get_py_params2 = _get_py_params({ fn: fn }, {}, { args: true }, arguments);

        var _get_py_params3 = _slicedToArray(_get_py_params2, 2);

        fn = _get_py_params3[0];
        args = _get_py_params3[1];

        var result = list();
        var _iteratorNormalCompletion5 = true;
        var _didIteratorError5 = false;
        var _iteratorError5 = undefined;

        try {
            for (var _iterator5 = izip_max.apply(null, args)[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                var item = _step5.value;

                var r = fn.apply(null, item);
                if (_lodash2.default.isUndefined(r)) r = null;
                result.append(r);
            }
        } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion5 && _iterator5.return) {
                    _iterator5.return();
                }
            } finally {
                if (_didIteratorError5) {
                    throw _iteratorError5;
                }
            }
        }

        return result;
    }
    PythonStd.map = map;

    function range(start) {
        var stop = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        var step = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

        if (start instanceof PyParams) {
            ;

            var _get_py_params4 = _get_py_params({ start, stop, step }, { stop: null, step: null }, { args: true, kwarg: false }, arguments);

            var _get_py_params5 = _slicedToArray(_get_py_params4, 3);

            start = _get_py_params5[0];
            stop = _get_py_params5[1];
            step = _get_py_params5[2];
        }if (stop === null) {
            // one param defined
            stop = start;
            start = 0;
        }

        if (step === null) {
            step = 1;
        }

        if (step > 0 && start >= stop || step < 0 && start <= stop) {
            return [];
        }

        var result = [];
        for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
            result.push(i);
        }
        return list(result);
    }
    PythonStd.range = range;

    function filter(lambda, iterable) {
        var result = list();
        var _iteratorNormalCompletion6 = true;
        var _didIteratorError6 = false;
        var _iteratorError6 = undefined;

        try {
            for (var _iterator6 = iterable[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                var element = _step6.value;

                var evaluation = lambda(element);
                if (evaluation) result.append(element);
            }
        } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion6 && _iterator6.return) {
                    _iterator6.return();
                }
            } finally {
                if (_didIteratorError6) {
                    throw _iteratorError6;
                }
            }
        }

        return result;
    }
    PythonStd.filter = filter;

    function reduce(fn, iterable) {
        var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

        if (_lodash2.default.isArray(iterable) || _lodash2.default.isMap(iterable)) if (_lodash2.default.size(iterable) == 0) throw new TypeError("reduce() of empty sequence with no initial value");
        if (iterable instanceof List || iterable instanceof Dict) if (iterable.__len__() == 0) throw new TypeError("reduce() of empty sequence with no initial value");

        var iterm = 0;
        var result = void 0;
        var _iteratorNormalCompletion7 = true;
        var _didIteratorError7 = false;
        var _iteratorError7 = undefined;

        try {
            for (var _iterator7 = iterable[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                var _value = _step7.value;

                if (iterm == 0) result = _value;else result = fn(_value, result);
                iterm = iterm + 1;
            }
        } catch (err) {
            _didIteratorError7 = true;
            _iteratorError7 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion7 && _iterator7.return) {
                    _iterator7.return();
                }
            } finally {
                if (_didIteratorError7) {
                    throw _iteratorError7;
                }
            }
        }

        if (value != null) result = fn(result, value);
        return result;
    }
    PythonStd.reduce = reduce;

    var Dict = function () {
        function Dict(params) {
            _classCallCheck(this, Dict);

            this.map = new Map();
            if (_lodash2.default.isUndefined(params)) return;
            var kwargs = void 0;

            var _get_py_params6 = _get_py_params({ params: params }, {}, { kwargs: true }, arguments);

            var _get_py_params7 = _slicedToArray(_get_py_params6, 2);

            params = _get_py_params7[0];
            kwargs = _get_py_params7[1];

            this.update(params);
            if (kwargs) this.update(kwargs);
        }

        _createClass(Dict, [{
            key: '__getitem__',
            value: function __getitem__(key) {
                return this.map.get(key);
            }
        }, {
            key: '__setitem__',
            value: function __setitem__(key, value) {
                return this.map.set(key, value);
            }
        }, {
            key: '__len__',
            value: function __len__() {
                return this.map.size;
            }
        }, {
            key: '__contains__',
            value: function __contains__(key) {
                return this.map.has(key);
            }
        }, {
            key: '__iter__',
            value: function __iter__() {
                return this.map.keys();
            }
        }, {
            key: '__eq__',
            value: function __eq__(other) {
                if (!(other instanceof Dict)) return false;
                if (this.map.size != other.__len__()) return false;
                var _iteratorNormalCompletion8 = true;
                var _didIteratorError8 = false;
                var _iteratorError8 = undefined;

                try {
                    for (var _iterator8 = this.map.entries()[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                        var _step8$value = _slicedToArray(_step8.value, 2),
                            key = _step8$value[0],
                            value = _step8$value[1];

                        if (!_eq(value, _item(other, key))) return False;
                    }
                } catch (err) {
                    _didIteratorError8 = true;
                    _iteratorError8 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion8 && _iterator8.return) {
                            _iterator8.return();
                        }
                    } finally {
                        if (_didIteratorError8) {
                            throw _iteratorError8;
                        }
                    }
                }

                return true;
            }
        }, {
            key: Symbol.iterator,
            value: function value() {
                return this.map.keys();
            }
        }, {
            key: 'keys',
            value: function keys() {
                return _lodash2.default.map(this.map.keys(), function (v) {
                    return v;
                });
            }
        }, {
            key: 'values',
            value: function values() {
                return _lodash2.default.map(this.map.values(), function (v) {
                    return v;
                });
            }
        }, {
            key: 'items',
            value: function items() {
                return _lodash2.default.map(this.map.entries(), function (v) {
                    return v;
                });
            }
        }, {
            key: 'entries',
            value: function entries() {
                return this.map.entries();
            }
        }, {
            key: 'update',
            value: function update(data) {
                var _this8 = this;

                if (!data) throw new TypeError("Empty or wrong parameters to method update");
                if (_lodash2.default.isPlainObject(data)) {
                    _lodash2.default.forOwn(data, function (value, key) {
                        _this8.map.set(key, value);
                    });
                } else if (data instanceof Map || data instanceof Dict) {
                    var _iteratorNormalCompletion9 = true;
                    var _didIteratorError9 = false;
                    var _iteratorError9 = undefined;

                    try {
                        for (var _iterator9 = data.entries()[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                            var _step9$value = _slicedToArray(_step9.value, 2),
                                key = _step9$value[0],
                                value = _step9$value[1];

                            this.map.set(key, value);
                        }
                    } catch (err) {
                        _didIteratorError9 = true;
                        _iteratorError9 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion9 && _iterator9.return) {
                                _iterator9.return();
                            }
                        } finally {
                            if (_didIteratorError9) {
                                throw _iteratorError9;
                            }
                        }
                    }
                } else {
                    var _iteratorNormalCompletion10 = true;
                    var _didIteratorError10 = false;
                    var _iteratorError10 = undefined;

                    try {
                        for (var _iterator10 = data[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
                            var _step10$value = _slicedToArray(_step10.value, 2),
                                key = _step10$value[0],
                                value = _step10$value[1];

                            this.map.set(key, value);
                        }
                    } catch (err) {
                        _didIteratorError10 = true;
                        _iteratorError10 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion10 && _iterator10.return) {
                                _iterator10.return();
                            }
                        } finally {
                            if (_didIteratorError10) {
                                throw _iteratorError10;
                            }
                        }
                    }
                }
            }
        }, {
            key: 'has',
            value: function has(key) {
                return this.map.has(key);
            }
        }, {
            key: 'get',
            value: function get(key, defvalue) {
                defvalue = defvalue || null;
                var result = this.map.get(key);
                if (result === undefined) return defvalue;
                return result;
            }
        }, {
            key: 'set',
            value: function set(key, value) {
                this.map.set(key, value);
            }
        }, {
            key: 'pop',
            value: function pop(key, defvalue) {
                if (!this.map.has(key)) {
                    if (_lodash2.default.isUndefined(defvalue)) throw KeyError("The dictionary has no key: " + key);
                    return defvalue;
                }
                var result = this.map.get(key);
                this.map.delete(key);
                return result;
            }
        }]);

        return Dict;
    }();

    PythonStd.Dict = Dict;

    function dict(params) {
        return new Dict(params);
    }
    PythonStd.dict = dict;

    var List = function () {
        function List(params) {
            _classCallCheck(this, List);

            this.list = [];
            if (_lodash2.default.isUndefined(params)) return;
            var kwargs = void 0;

            var _get_py_params8 = _get_py_params({ params: params }, {}, { kwargs: true }, arguments);

            var _get_py_params9 = _slicedToArray(_get_py_params8, 2);

            params = _get_py_params9[0];
            kwargs = _get_py_params9[1];

            this.update(params);
            if (kwargs) this.update(kwargs);
        }

        _createClass(List, [{
            key: Symbol.iterator,
            value: function value() {
                return this.__iter__();
            }
        }, {
            key: '__iter__',
            value: function __iter__() {
                //yield 444;
                return this.list.values();
            }
        }, {
            key: '__getitem__',
            value: function __getitem__(key) {
                if (key < 0) key = this.list.length + key;
                return this.list[key];
            }
        }, {
            key: '__setitem__',
            value: function __setitem__(key, value) {
                return this.list[key] = value;
            }
        }, {
            key: '__len__',
            value: function __len__() {
                return this.list.length;
            }
        }, {
            key: '__setslice__',
            value: function __setslice__(begin, end, values) {
                var object_length = this.list.length;

                if (begin === undefined || begin === null) begin = 0;
                if (end === undefined || end === null) end = object_length;

                var sub_begin = this.list.slice(0, begin);
                var sub_end = this.list.slice(end, this.list.length);
                this.list = [];
                this.list = this.list.concat(sub_begin);
                if (values instanceof Array || values instanceof List) {
                    var _iteratorNormalCompletion11 = true;
                    var _didIteratorError11 = false;
                    var _iteratorError11 = undefined;

                    try {
                        for (var _iterator11 = values.entries()[Symbol.iterator](), _step11; !(_iteratorNormalCompletion11 = (_step11 = _iterator11.next()).done); _iteratorNormalCompletion11 = true) {
                            var _step11$value = _slicedToArray(_step11.value, 2),
                                key = _step11$value[0],
                                value = _step11$value[1];

                            this.list.append(value);
                        }
                    } catch (err) {
                        _didIteratorError11 = true;
                        _iteratorError11 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion11 && _iterator11.return) {
                                _iterator11.return();
                            }
                        } finally {
                            if (_didIteratorError11) {
                                throw _iteratorError11;
                            }
                        }
                    }
                } else this.list = this.list.concat(values);

                this.list = this.list.concat(sub_end);
                return this.list;
            }
        }, {
            key: '__getslice__',
            value: function __getslice__(begin, end, step) {
                if (begin === undefined || begin === null) begin = 0;
                if (end === undefined || end === null) end = this.list.length;
                if (step === undefined || step === null) step = 1;
                var result = new List([]);
                for (var i = begin; i < end; i += step) {
                    result.append(this.list[i]);
                }return result;
            }
        }, {
            key: 'toString',
            value: function toString() {
                return this.list.toString();
            }
        }, {
            key: '__eq__',
            value: function __eq__(other) {
                if (!(other instanceof List)) {
                    return false;
                }
                if (this.list.length != other.__len__()) {
                    return false;
                }
                for (var i = 0; i < this.list.length; i++) {
                    if (!_eq(this.__getitem__(i), other.__getitem__(i))) {
                        return false;
                    }
                }
                return true;
            }
        }, {
            key: '__add__',
            value: function __add__(value) {
                this.update(value);
                return this;
            }
        }, {
            key: '__mul__',
            value: function __mul__(value) {
                var a_list = this.list;
                for (var i = 1; i < value; i++) {
                    this.list = this.list.concat(a_list);
                }return this;
            }
        }, {
            key: 'keys',
            value: function keys() {
                return this.list.keys();
            }
        }, {
            key: 'values',
            value: function values() {
                return this.list.values();
            }
        }, {
            key: 'items',
            value: function items() {
                return this.list;
            }
        }, {
            key: 'entries',
            value: function entries() {
                return this.list.entries();
            }
        }, {
            key: 'update',
            value: function update(data) {
                var _this9 = this;

                if (!data) throw new TypeError("Empty or wrong parameters to method update");
                if (_lodash2.default.isPlainObject(data)) {
                    _lodash2.default.forOwn(data, function (value) {
                        _this9.list.append(value);
                    });
                } else {
                    var _iteratorNormalCompletion12 = true;
                    var _didIteratorError12 = false;
                    var _iteratorError12 = undefined;

                    try {
                        for (var _iterator12 = data[Symbol.iterator](), _step12; !(_iteratorNormalCompletion12 = (_step12 = _iterator12.next()).done); _iteratorNormalCompletion12 = true) {
                            var _value2 = _step12.value;

                            this.list.append(_value2);
                        }
                    } catch (err) {
                        _didIteratorError12 = true;
                        _iteratorError12 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion12 && _iterator12.return) {
                                _iterator12.return();
                            }
                        } finally {
                            if (_didIteratorError12) {
                                throw _iteratorError12;
                            }
                        }
                    }
                }
            }
        }, {
            key: 'get',
            value: function get(key, defvalue) {
                defvalue = defvalue || null;
                var result = this.list.__getitem__(key);
                if (result === undefined) return defvalue;
                return result;
            }
        }, {
            key: 'set',
            value: function set(key, value) {
                this.list.__setitem__(key, value);
            }
        }, {
            key: 'pop',
            value: function pop(indexValue) {
                indexValue = indexValue || null;
                if (indexValue != null && indexValue != undefined) this.list.splice(indexValue, 1);else this.list.pop();
            }
        }, {
            key: 'append',
            value: function append(value) {
                value = value || null;
                if (_lodash2.default.isUndefined(value)) throw new TypeError("TypeError: append() takes exactly one argument (0 given)");else this.list.append(value);
            }
        }, {
            key: 'count',
            value: function count(value) {
                var count = 0;
                for (var i = 0; i < this.list.length; i++) {
                    if (_eq(this.list[i], value)) {
                        count = count + 1;
                    }
                }return count;
            }
        }, {
            key: 'extend',
            value: function extend(value) {
                var _iteratorNormalCompletion13 = true;
                var _didIteratorError13 = false;
                var _iteratorError13 = undefined;

                try {
                    for (var _iterator13 = value[Symbol.iterator](), _step13; !(_iteratorNormalCompletion13 = (_step13 = _iterator13.next()).done); _iteratorNormalCompletion13 = true) {
                        var item = _step13.value;

                        this.list.append(item);
                    }
                } catch (err) {
                    _didIteratorError13 = true;
                    _iteratorError13 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion13 && _iterator13.return) {
                            _iterator13.return();
                        }
                    } finally {
                        if (_didIteratorError13) {
                            throw _iteratorError13;
                        }
                    }
                }
            }
        }, {
            key: 'index',
            value: function index(searchValue, startValue, stopValue) {
                startValue = startValue || 0;
                stopValue = stopValue || this.list.length;
                for (var i = startValue; i < stopValue; i++) {
                    if (_eq(this.list[i], searchValue)) return i;
                }throw new ValueError(searchValue.toString() + " is not in list");
            }
        }, {
            key: 'insert',
            value: function insert(indexValue, insertValue) {
                this.list.splice(indexValue, 0, insertValue);
            }
        }, {
            key: 'remove',
            value: function remove(removeValue) {
                for (var i = 0; i < this.list.length; i++) {
                    if (_eq(this.list[i], removeValue)) {
                        this.list.splice(i, 1);
                        return;
                    }
                }return new ValueError("list.remove(x): x not in list");
            }
        }, {
            key: 'reverse',
            value: function reverse() {
                this.list.reverse();
            }
        }, {
            key: 'sort',
            value: function sort() {
                this.list.sort();
            }
        }]);

        return List;
    }();

    PythonStd.List = List;

    function list(params) {
        return new List(params);
    }
    PythonStd.list = list;

    if (typeof window === 'undefined') module.exports = PythonStd;else {
        window.pythonstd = Object.assign({}, PythonStd);
    }
})();